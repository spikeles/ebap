package au.id.gar;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.*;
import javax.tools.Diagnostic.Kind;
import javax.tools.FileObject;
import javax.tools.JavaFileManager.Location;
import javax.tools.StandardLocation;

/**
 * Create a resource file containing the classes that use the EventSubscriber annotation.
 * 
 * @author Glen Ritchie ( glensr@gar.id.au )
 * 
 *         Copyright 2013
 *         You may use this file for whatever you desire as long I'm kept as the original author.
 */
@SupportedSourceVersion(SourceVersion.RELEASE_7)
@SupportedAnnotationTypes(value = "org.bushe.swing.event.annotation.EventSubscriber")
public class EventBusAnnotationProcessor extends AbstractProcessor
{
	/** True outputs info to the logs */
	final static boolean DEBUG = false;

	/** Resource file to write to */
	private Writer writer;

	@Override
	public synchronized void init(final ProcessingEnvironment env)
	{
		super.init(env);

		Location location = StandardLocation.SOURCE_OUTPUT;
		String pkg = "au.id.gar.annotations";
		String relativeName = "eventbus.classes";
		try
		{
			FileObject resourceFile = env.getFiler().createResource(location, pkg, relativeName);
			writer = resourceFile.openWriter();
		} catch (IOException ex)
		{
			processingEnv.getMessager()
					.printMessage(Kind.ERROR, "Failed annotation processing:" + ex.getMessage());
		}
	}

	@Override
	public boolean process(final Set<? extends TypeElement> annotations, final RoundEnvironment roundEnv)
	{
		try
		{
			// Close the writer if processing is over
			if (roundEnv.processingOver())
				writer.close();

			for (TypeElement annotation : annotations)
			{
				Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(annotation);
				for (Element e : elements)
				{
					try
					{
						if (DEBUG)
						{
							String str = "GenCode[" + e.getSimpleName() + "], "
									+ e.getEnclosingElement().toString() + ":" + e.getClass();
							processingEnv.getMessager()
									.printMessage(Kind.NOTE, str, e);
						}

						/** If it's an executable element(it should be), do some extra checks */
						if (e instanceof ExecutableElement)
						{
							ExecutableElement ee = (ExecutableElement) e;
							List<? extends VariableElement> params = ee.getParameters();
							// If we have zero params this is bad
							// TODO: Check if param is  primitive 
							if (params.size() == 0)
							{
								// Try to guess what they meant, lets grab the "eventClass" value and suggest that.
								List<? extends AnnotationMirror> annotationMirrors = e.getAnnotationMirrors();
								String hint = "Maybe you meant ";
								if (annotationMirrors.size() > 0)
								{
									for (AnnotationMirror m : annotationMirrors)
									{
										if (m.toString().contains("EventSubscriber"))
										{
											Map<? extends ExecutableElement, ? extends AnnotationValue> values = m
													.getElementValues();
											for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : values
													.entrySet())
											{
												ExecutableElement key = entry.getKey();
												AnnotationValue value = entry.getValue();
												if (key.getSimpleName().toString().trim().equals("eventClass"))
												{
													hint += " " + value.getValue() + " ?";
												}
											}
										}

									}
								}
								// Tell the processing environment we have an error
								processingEnv.getMessager()
										.printMessage(
												Kind.ERROR,
												"Annotation requires method have at least one non-primitive argument. "
														+ hint,
												e);
							}

						}

						// Convert the class of method we are looking at into a string and write it to the file
						String classname = e.getEnclosingElement().toString();
						writer.write(classname + "\n");

					} catch (Exception ex)
					{
						processingEnv
								.getMessager()
								.printMessage(Kind.ERROR,
										"Failed annotation for:" + e.getSimpleName() + ":" + ex, e);
					}

				}
			}
		} catch (Exception ex)
		{
			processingEnv.getMessager()
					.printMessage(Kind.ERROR, "Failed annotation processing:" + ex.getMessage());
		}
		return true;
	}
}
